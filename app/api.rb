module  AuthSec
  class Api < Grape::API
    use GrapeLogging::Middleware::RequestLogger, 
      logger: AuthSec.logger,
      formatter: ::AuthSec::JsonFormatter.new

    format :json
    helpers do
      def logger
        AuthSec.logger
      end
    end

    desc "API root path"
    get "/" do
      { status: "Welcome to the auth secure API" }
    end

    mount AuthSec::HealthCheck
  end
end

