module AuthSec

  Status = Struct.new(:status)

  class HealthCheck < Grape::API

    desc "return a public health status"
    resource :health do
      get :all do
        status = AuthSec::Status.new("ok")
        present status, with: AuthSec::HealthCheckEntity
      end
    end
  end
end
