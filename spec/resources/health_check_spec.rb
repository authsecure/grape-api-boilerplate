require "spec_helper"

describe  AuthSec::HealthCheck do

  let(:response) {
    { "status" =>  "ok" } 
  }
  
  context "/health/all" do
    it "returns 200 code" do
      get '/health/all'
      expect(JSON.parse(last_response.body)).to eql(response)
      expect(last_response).to be_ok
    end
  end
end
