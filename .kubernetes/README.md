## Kubernetes Tasks:
Some scripts have been added to the `bin` directory. These will help you execute
migrations, run tests, and other tasks in the context of the application
(data storage, envs, etc...).

- `bin/kube_init`

  Registers the app and it's dependencies with kubernetes.

- `bin/kube_hard_reset`

  Unregisters the app and it's dependencies with kubernetes. Then runs
  `bin/kube_init`.

- `bin/kube_bexec`

  This executes the `bundle exec` command in the kubernetes env, so it gives you
  access to kuberntes components like dns, env variables, etc.

  For example, running the usual `bundle exec rake db:migrate` in your
  terminal will have a connection db error since you cannot access the
  database.

  Instead, use:

  ```sh
  bin/kube_bexec rake db:migrate
  ```

  To execute your specs :

  ```sh
  bin/kube_bexec rake db:test:prepare ## to prepare the database test
  bin/kube_bexec rspec
  ```

- `bin/kube_refresh_app`

  If you modify your initializers, you can refresh the whole application by
  calling this command.

- `bin/kube_reset_app`

  Deletes and recreates the application in the kubernetes cluster.
- ./bin/kube_sandbox

   This gives you ability to deploy your local version to the sandbox env
```
 ./bin/kube_sandbox -h # to see options
 ./bin/kube_sandbox -p .kubernetes/templates/lbe.yaml # to deploy
```

## `bin/kube`

Kubernetes pods can have different names in different contexts, so this script
will help you work with them without having to `get pods` and copy paste the
right names.

It has two modes, the first one is just a shortcut to `kubectl --context=x`,
the second one (the `--` mode) is a shortcut to our custom `bin/kube-*` commands).

Kubectl environments (contexts) must be set up before this command is used
(see [configuration instructions][kube-context-config]). The command offers
the following aliases for `CONTEXT`:
  * `staging` and `qa` will set `--context=aws-staging`
  * `sandbox` and `dev` will set `--context=aws-dev`
  * `local` will not pass `--context` at all

### Subcommands

- `bin/kube CONTEXT -- rails-console`

  Will run `rails console` on the given `CONTEXT` (can be `local`, `sandbox`, `qa`).

- `bin/kube CONTEXT -- tty`

  This gives you the ability to open an ssh connection to the container hosting
  the application. `CONTEXT` is same as above.
  
- `bin/kube CONTEXT -- logs`

  Will tail logs for given `CONTEXT` (see above). If an environment has multiple pods, logs will be tailed and merged into a single stream.


### Examples

    bin/kube qa get pods
    bin/kube staging -- rails-console
    bin/kube local -- tty
    bin/kube qa -- logs

[kube-context-config]: https://skillsoftdev.atlassian.net/wiki/display/S3/Kubernetes+Deployment
