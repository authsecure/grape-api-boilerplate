# Grape API Boilerplate

### Directory Tree

```

├── Dockerfile
├── Gemfile
├── Gemfile.lock
├── README.md
├── Rakefile
├── app
│   ├── api.rb
│   ├── entities
│   │   └── health_check_entity.rb
│   ├── jobs
│   │   └── checker.rb
│   ├── jobs.rb
│   ├── models
│   │   └── check.rb
│   └── resources
│       └── health_check.rb
├── bin
│   └── start
├── config
│   ├── application.rb
│   └── env.rb
├── config.ru
├── lib
│   ├── config.rb
│   ├── logger
│   │   └── json_formatter.rb
│   └── progress_bar.rb
├── script
│   ├── console
│   ├── runner
│   └── seeds.rb
└── spec
    ├── factories
```