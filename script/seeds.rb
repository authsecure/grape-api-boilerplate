#!/usr/bin/env ruby
require 'pry'
require 'bundler'
root = File.expand_path '..', __dir__

ENV['RBENV_DIR'] = root
ENV['BUNDLE_GEMFILE'] = root + '/Gemfile'
ENV['RACK_ENV'] = ARGV[0] || 'development'

Bundler.setup
require_relative '../config/application'

