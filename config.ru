require 'rack/cors'
require './config/application.rb'
require 'grape_logging'

use Rack::Cors do
  allow do
    origins '*'
    resource '*', :headers => :any, :methods => [:get, :patch, :post, :delete]
  end
end

run AuthSec::Api.new
