require "grape"
require "grape-entity"
require 'grape_logging'
require "active_support/inflector"
require "active_support"

module AuthSec
  ENV["RACK_ENV"] ||= "development"
  api_env = ENV["RACK_ENV"]

  def self.autoload_dir dir
    Dir.glob("#{dir}/*.rb").each do |file_name|
      _module = File.basename(file_name, '.rb').camelize.to_sym
      autoload _module, file_name
    end
  end

  def self.logger
    @logger ||= Logger.new(STDOUT) 
    @logger.formatter = ::AuthSec::JsonFormatter.new
    @logger
  end

  class Application

    def self.root_path
      @root = File.expand_path(File.join(__dir__, '/..'))
    end

    def initialize
      load_libs
      configuration = AuthSec::Configuration.load
    end

    def load_libs
      AuthSec.autoload_dir File.expand_path File.join("app")
      %w(entities models resources).each do |dir_name|
        AuthSec.autoload_dir File.expand_path File.join("app", dir_name)
      end
      AuthSec.autoload_dir File.expand_path File.join("lib")
      AuthSec.autoload_dir File.expand_path File.join("lib", "logger")
    end
  end

  module Configuration
    require "./config/env"
    def self.load
      ::AuthSec::Env.load
    end
  end
end

AuthSec::Application.new
