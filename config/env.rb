require 'ostruct'

module AuthSec
  module Env

    class << self
      def load
        AuthSec::Config.init_envs do |cf|
          cf.env(:rack_env, ENV["RACK_ENV"])
          cf.env(:databse_user, ENV["POSTGRES_USER"])
          cf.env(:databse_password, ENV["POSTGRES_PASSWORD"])
          cf.env(:databse_host, ENV["POSTGRES_HOST"])
        end
      end

      def current
        OpenStruct.new(AuthSec::Config.envs)
      end
    end
  end
end
