FROM library/ruby:2.4.0
MAINTAINER Tarik Ihadjadene

# Prepare the work directory
VOLUME ["/app", "/.bundle"]
WORKDIR /app

RUN gem install bundler
RUN bundle config path /.bundle

# Add the application entrypoint

CMD /app/start


