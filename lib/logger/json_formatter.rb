require 'grape_logging'
require 'json'
module AuthSec
  class JsonFormatter < ::GrapeLogging::Formatters::Default

    def call(severity, datetime, _, data)
      @severity = severity
      @datetime = datetime
      format(data)
    end

    def format(data)
      case data
      when Exception
        format_exception(data)
      when Hash
        format_hash(data)
      else
        format_default(data)
      end
    end

    private
    def format_default(data)
      log = { 
        datetime: @datetime.to_s,
        severity: @severity,
        message: data
      }
      JSON.dump(log).concat("\n")
    end

    def format_hash(data)
      data = { message: data }
      data.merge(datatime: @datetime.to_s, severity: @severit)
      JSON.dump(data).concat("\n")
    end

    def format_exception(data)
      log = {
        datetime: @datetime.to_s, 
        severity: @severity,
        message: super(data)
      }
      JSON.dump(log).concat("\n")
    end
  end
end

