module AuthSec
  module Config
    extend self

    def init_envs(&block) 
      block.call(self)
    end

    def env(key, value)
      envs[key] = value
    end

    def envs
      @envs ||= {}
    end
  end
end
